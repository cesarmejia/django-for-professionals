# Django for Professionals

<!-- TOC -->

- [Django for Professionals](#django-for-professionals)
  - [Outline](#outline)
  - [Introduction](#introduction)
  - [Chapter 1: Docker](#chapter-1-docker)
  - [Chapter 2: PostgreSQL](#chapter-2-postgresql)
  - [Chapter 3: Bookstore Project](#chapter-3-bookstore-project)
  - [Chapter 4: Pages App](#chapter-4-pages-app)

<!-- /TOC -->

## Outline

- [x] ~~_Introduction_~~ [2020-07-17]
- [x] ~~_Chapter 1: Docker_~~ [2020-07-18]
- [x] ~~_Chapter 2: PostgreSQL_~~ [2020-07-19]
- [x] ~~_Chapter 3: Bookstore Project_~~ [2020-07-19]
- [x] ~~_Chapter 4: Pages App_~~ [2020-07-19]
- [ ] Chapter 5: User Registration
- [ ] Chapter 6: Static Assets
- [ ] Chapter 7: Advanced User Registration
- [ ] Chapter 8: Environment Variables
- [ ] Chapter 9: Email
- [ ] Chapter 10: Books App
- [ ] Chapter 11: Reviews App
- [ ] Chapter 12: File/Image Uploads
- [ ] Chapter 13: Permissions
- [ ] Chapter 14: Orders with Stripe
- [ ] Chapter 15: Search
- [ ] Chapter 16: Performance
- [ ] Chapter 17: Security
- [ ] Chapter 18: Deployment
- [ ] Conclusion

## Introduction

- No notes

## Chapter 1: Docker

- Two important files:

  1. `Dockerfile`
  2. `docker-compose.yml`

- Python virtual environment tools (only isolate Python packages):

  1. `virtualenv`
  2. `venv`
  3. `Pipenv`

- Learn Docker -> [Dive into Docker video course](https://diveintodocker.com/ref-dfp)

- `Docker Compose` is a tool for defining and running multi-container Docker applications.

- Steps in setting up Django within a Docker container:
  1. Create a virtual environment locally and install Django
  2. Create a new project
  3. Exit the virtual environment
  4. Write a `Dockerfile` and then build the initial image
  5. Write a `docker-compose.yml` file and run the container with `docker-compose up`

## Chapter 2: PostgreSQL

- `psycopg2` is a database adapter (allows python to talk to sql db)

  - Pronounced "psycho-p-g"

- Steps when installing new packages:

  1. Install new package within Docker (`docker-compose exec pipenv install [package]`)
  2. Stop running containers
  3. Force an image rebuild & restart containers (`docker-compose up --build`)

- To start container in the background run: `docker-compose up -d` (-d for "detached")
- If running containers in the background, you can log output with `docker-compose logs`
- Database setup within `docker-compose.yml` requires `POSTGRES_PASSWORD` environmental variable to be setup

  ```yml
  db:
  image: postgres:11
  environment:
    POSTGRES_PASSWORD: 'postgres'
  ```

## Chapter 3: Bookstore Project

- Setting up a `CustomUser` model

  - Before Django 1.5, setup a `OneToOneField` to a `Profile` model to `User` model
  - After Django 1.5, setup a `CustomUser` model
    1. Either extend `AbstractUser` (keeps the default User fields and permissions)
    2. Or extend `AbstractBaseUser` (more granular control and flexible but harder)

- Four steps to add a `CustomUser` model

  1. Create a `CustomUser` model
  2. Update `settings.py`
  3. Customize `UserCreationForm` and `UserChangeForm`
  4. Add the custom user model to admin.py

- _"Code without tests is broken as designed."_
  - Jacob Kaplan-Moss

## Chapter 4: Pages App

- Covers templates, `SimpleTestCase` (doesn't provide model methods) and url_methods `resolve` and `reverse`
- Also covers `setUp` method to help keep tests DRY

## Chatper 5: User Registration
